pub struct Node {
}

impl Node {
    pub fn receive(&self, msg: &str) {
        println!("{}", msg);
    }
    pub fn receive_confirm<F>(&self, msg: &str, mut f: F) where
        F: FnMut() {
        println!("{}", msg);
        f();
    }
}
