#![feature(plugin)]
#![plugin(clippy)]

extern crate glium;

use glium::glutin;

fn main() {
    let mut events_loop = glutin::EventsLoop::new();
    let _window = glutin::WindowBuilder::new();
    let context = glutin::ContextBuilder::new();
    let _display = glium::Display::new(_window, context, &events_loop).unwrap();

    let mut closed = false;
    while !closed {
        // listing the events produced by application and waiting to be received

        events_loop.poll_events(|ev| {

            if let glutin::Event::WindowEvent { event, .. } = ev { 
                match event {
                    glutin::WindowEvent::Closed => closed = true,
                    _ => (),
                } 
            }
        });
    }    
}
