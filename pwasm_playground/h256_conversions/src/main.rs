use fixed_hash::construct_fixed_hash;

construct_fixed_hash!{
    /// A 256-bits (32 bytes) hash type.
    pub struct H256(32);
}

fn main() {
    println!("Hello, world!");
}

fn write_string(storage: &mut Vec<H256>, token_name: &[u8]) {
    storage.push(H256::from_low_u64_le(token_name.len() as u64));

    // write chunked u8 values into H256 blocks
    let chunks = token_name.chunks(32);
    for c in chunks {
        // initialize to 0
        let mut chunk = [0u8; 32];
        // copy relevant bytes to H256
        chunk[..c.len()].copy_from_slice(c);
        // push H256 to storage
        storage.push(H256::from(chunk));
    }
}

fn read_string(storage: &Vec<H256>) -> Vec<u8> {
    let mut reconstructed: Vec<u8> = Vec::new();
    let mut remaining = storage[0].to_low_u64_le() as usize;
    let mut idx = 1;
    while remaining > 0 {
        let to_read = if remaining >= 32 {32} else {remaining};
        reconstructed.extend_from_slice(&storage[idx].as_ref()[..remaining]);
        remaining -= to_read;
        idx += 1;
    }
    reconstructed
}


#[cfg(test)]
mod test {
    use super::*;
    use core::str::FromStr;

    #[test]
    fn test_from_str() {
        // FromStr trait implementation in fixed_hash is dependent on the rustc-hex feature.
        // rustc-hex only optionally depends on std, which means it can be available in non-std environments.
        H256::from_str("000000000000000000000000000000000000000000000000000000000000000f").unwrap();
    }

    #[test]
    fn test_string_to_h256() {
        let token_name = "Test".as_bytes();

        let mut storage: Vec<H256> = Vec::new();
        write_string(&mut storage, token_name);
        let reconstructed = read_string(&storage);

        assert_eq!(token_name.len(), reconstructed.len());
        assert_eq!(token_name.to_vec(), reconstructed);
    }
}
