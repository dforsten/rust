#![feature(plugin)]
#![plugin(clippy)]

// globally imported crates
extern crate rand;

// global definitions
static LOSS_RATIO: f64 = 0.1;

mod node;
mod naive_network;
mod acknowledging_network;

fn main() {
    naive_message_passing();
    message_passing_with_acknowledgements();
}

fn naive_message_passing() {
    // Model 2.4
    // In the message passing model with message loss, for any specific message,
    // it is not guaranteed that it will arrive safely at the receiver
    let n1 = node::Node{};
    for n in 1..101 {
        naive_network::send(&n1, &n.to_string());
    }
}

fn message_passing_with_acknowledgements() {
    // Algorithm 2.5
    // Client sends commands one at a time to the server
    // Server acknowledges every command
    // If the client does not receive an acknowledgement within reasonable time,
    // the client re-sends the command.
    let n1 = node::Node{};
    let mut network = acknowledging_network::Network::new();
    for n in 1..101 {
        network.send(&n1, &n.to_string());
    }
}
