    #[allow(dead_code)]
    #[allow(missing_docs)]
    #[allow(unused_imports)]
    #[allow(unused_mut)]
    #[allow(unused_variables)]
    pub mod streem_acl {
        #[ethabi_contract_options(name = "StreemAcl",
                                  path = "res/contracts/streem.json")]
        struct _Dummy;
        use ethabi;
        const INTERNAL_ERR: &'static str = "`ethabi_derive` internal error";
        #[doc = r" Contract"]
        #[structural_match]
        #[rustc_copy_clone_marker]

        pub struct StreemAcl {
        }

        impl StreemAcl { }
        
        pub mod functions {
            use ethabi;

            pub struct Increment {
                function: ethabi::Function,
            }
            impl Default for Increment {
                fn default() -> Self {
                    Increment{function:
                                  ethabi::Function{name:
                                                       "increment".to_owned(),
                                                   inputs:
                                                       <[_]>::into_vec(box
                                                                           []),
                                                   outputs:
                                                       <[_]>::into_vec(box
                                                                           []),
                                                   constant: false,},}
                }
            }            
            impl Increment {
                pub fn input(&self) -> ethabi::Bytes {
                    let v: Vec<ethabi::Token> = <[_]>::into_vec(box []);
                    self.function.encode_input(&v).expect(super::INTERNAL_ERR)
                }
            }

            pub struct Set {
                function: ethabi::Function,
            }
            impl Default for Set {
                fn default() -> Self {
                    Set{function:
                            ethabi::Function{name: "set".to_owned(),
                                             inputs:
                                                 <[_]>::into_vec(box
                                                                     [ethabi::Param{name:
                                                                                        "newState".to_owned(),
                                                                                    kind:
                                                                                        ethabi::ParamType::Uint(32usize),}]),
                                             outputs: <[_]>::into_vec(box []),
                                             constant: false,},}
                }
            }
            impl Set {
                pub fn input<T0: Into<ethabi::Uint>>(&self, new_state: T0)
                 -> ethabi::Bytes {
                    let v: Vec<ethabi::Token> =
                        <[_]>::into_vec(box
                                            [ethabi::Token::Uint(new_state.into())]);
                    self.function.encode_input(&v).expect(super::INTERNAL_ERR)
                }
            }

            pub struct Get {
                function: ethabi::Function,
            }
            impl Default for Get {
                fn default() -> Self {
                    Get{function:
                            ethabi::Function{name: "get".to_owned(),
                                             inputs: <[_]>::into_vec(box []),
                                             outputs:
                                                 <[_]>::into_vec(box
                                                                     [ethabi::Param{name:
                                                                                        "".to_owned(),
                                                                                    kind:
                                                                                        ethabi::ParamType::Uint(32usize),}]),
                                             constant: true,},}
                }
            }            
            impl Get {
                pub fn input(&self) -> ethabi::Bytes {
                    let v: Vec<ethabi::Token> = <[_]>::into_vec(box []);
                    self.function.encode_input(&v).expect(super::INTERNAL_ERR)
                }
                pub fn output(&self, output: &[u8])
                 -> ethabi::Result<ethabi::Uint> {
                    let out =
                        self.function.decode_output(output)?.into_iter().next().expect(super::INTERNAL_ERR);
                    Ok(out.to_uint().expect(super::INTERNAL_ERR))
                }
                pub fn call(&self,
                            do_call:
                                &Fn(ethabi::Bytes) ->
                                 Result<ethabi::Bytes, String>)
                 -> ethabi::Result<ethabi::Uint> {
                    let encoded_input = self.input();
                    do_call(encoded_input).map_err(|x|
                                                       ethabi::Error::with_chain(ethabi::Error::from(x),
                                                                                 ethabi::ErrorKind::CallError)).and_then(|encoded_output|
                                                                                                                             self.output(&encoded_output))
                }
            }
        }

        #[structural_match]
        #[rustc_copy_clone_marker]
        pub struct StreemAclFunctions {
        }
        impl StreemAclFunctions {
            pub fn increment(&self) -> functions::Increment {
                functions::Increment::default()
            }
            pub fn set(&self) -> functions::Set { functions::Set::default() }
            pub fn get(&self) -> functions::Get { functions::Get::default() }
        }
        impl StreemAcl {
            pub fn functions(&self) -> StreemAclFunctions {
                StreemAclFunctions{}
            }
        }
    }
}
