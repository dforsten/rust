use rand::Rng;

pub fn send(node: &::node::Node, msg: &str) {
    let secret_number = ::rand::thread_rng().gen::<f64>();
    if secret_number > ::LOSS_RATIO {
        node.receive(msg);
    } else {
        println!("Dropped message: {}", msg);
    }
}
