
use rand::Rng;
use std::collections::VecDeque;

pub struct Network {
    pub unconfirmed: VecDeque<String>
}

impl Network {
    pub fn new() -> Network {
        let unconfirmed: VecDeque<String> = VecDeque::new();
        Network { unconfirmed }
    }

    pub fn send(&mut self, node: &::node::Node, msg: &str) {
        self.resend_unconfirmed();
        self.unconfirmed.push_back(String::from(msg));
        self.do_send(node, msg);
    }

    fn resend_unconfirmed(&mut self) {
        // Implement sending of unconfirmed,
        // needs to store the node to send to as well.
    }

    fn do_send(&mut self, node: &::node::Node, msg: &str) {
        let probability = ::rand::thread_rng().gen::<f64>();            
        if probability > ::LOSS_RATIO {            
            node.receive_confirm(msg, || self.confirm());
        } else {
            println!("Dropped message: {}", msg);
        }
    }

    fn confirm(&mut self) {
        assert!(!self.unconfirmed.is_empty(), "Double Acknowledge?");
        self.unconfirmed.pop_front();
    } 
}
