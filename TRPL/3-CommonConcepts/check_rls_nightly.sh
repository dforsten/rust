#!/usr/bin/env bash
# Update rust nightly as long as rls is included
if curl https://static.rust-lang.org/dist/channel-rust-nightly.toml 2>/dev/null | grep -q 'rls-preview' 
then
  rustup update nightly
else
  echo 'latest nightly is missing rls' >&2
  exit 1
fi
