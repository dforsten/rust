extern crate ethabi;
extern crate ethereum_types;
extern crate tiny_keccak;

use ethabi::{Address, Token};
use ethereum_types::U256;

#[macro_use]
extern crate macros_procedural;

macro_rules! builtin_contract {
    ($module:ident, $name:expr, $path:expr) => {
        #[allow(dead_code)]
        #[allow(missing_docs)]
        #[allow(unused_imports)]
        #[allow(unused_mut)]
        #[allow(unused_variables)]
        pub mod $module {
            #[derive(BuiltinContractWrapper)]
            #[builtin_contract_options(name = $name, path = $path)]
            struct _Dummy;
        }
    };
}

builtin_contract!(
    builtin_streem_acl,
    "BuiltinStreemAcl",
    "res/contracts/streem.json"
);

fn convert_to(token: &Token) -> U256 {
    match token {
        &Token::Uint(val) => val,
        _ => U256::from(0),
    }
}

fn convert_to_address(token: &Token) -> Address {
    match token {
        &Token::Address(val) => val,
        _ => Address::from(0),
    }
}

fn print_something<T>(x: T)
where
    T: std::fmt::Debug,
{
    println!("{:?}", x);
}

fn test_into<T0: Into<ethabi::Uint>>(new_state: T0) {
}

fn main() {
    let var1 = Token::Uint(U256::from(3));
    println!("{}", convert_to(&var1));

    /// TBD: test "test_into"

    let var2 = Token::Address(Address::from(5));
    println!("{}", convert_to_address(&var2));
    print_something(var2);
}
