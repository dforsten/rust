fn main() {
    println!("Hello, world!");
}

#[cfg(test)]
mod tests {
    use uint::U256;
    use std::str::FromStr;

    #[test]
    fn test_from_str() {
        // Notes:
        // "from_str" requires using the "fromStr" trait - usually from the std::str module.
        // The implementation of FromStr is dependent on the "std" feature in the uint crate,
        // and therefore *not* available in a no-std environment.
        // Conversions from numeric types like u64 and i64 however *are* available

        U256::from_str("000000000000000000000000000000000000000000000000000000000000000f").unwrap();
    }

    #[test]
    fn test_from_u64() {
        // available in both std and no-std environments
        U256::from(1u64);
    }
}