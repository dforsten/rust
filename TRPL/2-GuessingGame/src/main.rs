#![feature(plugin)]
#![plugin(clippy)]

extern crate rand;

use std::io;
use std::cmp::Ordering;
use rand::Rng;

fn main() {
    println!("Guess the number!");

    let secret_number = rand::thread_rng().gen_range(1, 101);

    println!("The secret number is: {}", secret_number);

    // infinite loop
    loop {
        println!("Please input your guess.");
        let mut guess = String::new();

        // read_line returns a "Result" Enum,
        // either the "Ok" element or the "Err" element.
        // Both "Ok" or "Err" provide a "expect" function,
        // in the case of "Ok" it does nothing but returning
        // the length of the read line. In the case of "Err"
        // the "expect" function throws an error with the given
        // text.
        io::stdin().read_line(&mut guess)
            .expect("Failed to read line");

        println!("You guessed: {}", guess);

        // Shadows the "String" type variable already named "guess"
        // We are using "match" instead of calling "expect"
        // Match can return a value in addition to executing code in each "arm".
        let guess: u32 = match guess.trim().parse() {
            // Extracts the value from Ok and returns it
            Ok(num) => num,
            // "_" is the "catchall" value, just continues at the beginning of the loop
            Err(_) => continue,
        };

        match guess.cmp(&secret_number) {
            Ordering::Less    => println!("Too small!"),
            Ordering::Greater => println!("Too big!"),
            Ordering::Equal   => {
                println!("You win!");
                // exit the otherwise infinite loop
                break;
            }
        }
    }
}
