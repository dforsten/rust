#![feature(plugin)]
#![plugin(clippy)]

fn main() {
    println!("Integer literals:");

    // Integers are 32bit signed by default
    let i = 12;
    println!("let i = 12 => {}", i);

    // "mut" is not allowed with "const"
    // const type *must* be annotated
    // clippy: use "_" as separator for long integer literals
    const I: u32 = 100_000;
    println!("const I: u32 = 100_000 => {}", I);

    let h = 0xff;
    let o = 0o77;
    println!("Hex and Octal numbers as usual in C/C++ {} {}", h, o);

    // clippy: no separators needed for long floating point values
    let d = 0.5334242353;
    println!("Floating point variables are *double* by default in Rust: {}", d);

    // tuples are immutable, ever member can be of different type
    let tp = (4, 6.53, 'b');
    println!("A tuple using three different types: {} {} {}", tp.0, tp.1, tp.2);

    // extract values from tuples using patterns
    let (xx, yy, zz) = tp;
    println!("Values extracted using patterns: {} {} {}", xx, yy, zz);

    // arrays are immutable like tuples, but must be of the same type
    let ar = [5, 3, 2, 6];
    println!("Arrays must be of same type: {}", ar[2]);

    // Functions
    let mystring = String::from("blah");

    println!("This is a string: {}", mystring);

    print_string(&mystring);

    enums();
}

fn print_string(s: &str) {
    println!("print_string: {}", s);
}

enum IpAddr {
    V4(u8, u8, u8, u8),
    V6(String)
}

fn enums() {
    let four = IpAddr::V4(127, 0, 0, 1);
    let six = IpAddr::V6(String::from("::1"));
    route(&four);
    route(&six);
}

fn route(ip_type: &IpAddr) {
    // Dereferencing in the match statement to avoid having to use
    // references for every single arm.
    match *ip_type {
        IpAddr::V4(..) => println!("routing IPv4"),
        IpAddr::V6(..) => println!("routing IPv6"),
    }
}
